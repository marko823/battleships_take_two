package Battleships;

import java.util.Random;

public class NumberGenerator {
	private Random pos;

	public NumberGenerator() {
		pos = new Random();
	}

	public int rand(int range) {

		int x = pos.nextInt(range);

		if (x < 0)
			x = -x;

		return (x);
	}

}
