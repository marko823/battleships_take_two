package Battleships.MouseAdapters;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import Battleships.Coordinate;
import Battleships.GUI;
import Battleships.Graphics.Panel;

public class AttackMousePressListener extends MouseAdapter {

	private Panel a;
	private GUI gui;

	public AttackMousePressListener(Panel p, GUI gui2) {
		a = p;
		gui = gui2;
	}

	public void mousePressed(MouseEvent event) {
		if (gui.getGameState().IsAcceptingPlayerInput()) {

			Coordinate c = new Coordinate(GUI.resolveAxisCoord(event.getY()),
					GUI.resolveAxisCoord(event.getX()));

			System.out.println(c.getI() + " " + c.getJ());

			gui.getGameState()
					.acceptPlayerShot(c, a.getGraphics(), gui.outText);

		}
	}

}
