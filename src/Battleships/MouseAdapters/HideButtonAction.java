package Battleships.MouseAdapters;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import Battleships.GUI;

public class HideButtonAction extends MouseAdapter {

	private GUI gui;

	public HideButtonAction(GUI gui) {
		this.gui = gui;
	}

	public void mousePressed(MouseEvent event) {
		gui.hideMap();
	}

}
