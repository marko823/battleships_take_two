package Battleships.MouseAdapters;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import Battleships.GUI;

public class RotateButtonAction extends MouseAdapter {

	private GUI gui;

	public RotateButtonAction(GUI gui) {
		this.gui = gui;
	}

	public void mousePressed(MouseEvent event) {
		gui.rotate();
		System.out.println("Horiz = " + gui.guiDomain.horiz);
	}
	
	

}