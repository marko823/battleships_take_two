package Battleships.MouseAdapters;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import Battleships.Coordinate;
import Battleships.GUI;
import Battleships.Graphics.Panel;

public class HomeMousePressListener extends MouseAdapter {

	private Panel a;
	private GUI gui;

	public HomeMousePressListener(Panel p, GUI gui2) {
		a = p;
		gui = gui2;
	}

	public void mousePressed(MouseEvent event) {

		Graphics g = a.getGraphics();

		Coordinate c = new Coordinate(GUI.resolveAxisCoord(event.getY()),
				GUI.resolveAxisCoord(event.getX()));
		System.out.println(c.getI() + " " + c.getJ());

		if (!gui.getGameState().allShipsDeployed()) {
			gui.getGameState().playerHomeGrid.deploy(c, gui.guiDomain.horiz,
					gui.homePanel, gui.outText, gui);

		}
		System.out.println("Element corresponds to " + c);
		// repaint();
	}

}
