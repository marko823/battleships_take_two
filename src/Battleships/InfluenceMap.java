package Battleships;

import java.io.Serializable;

import Battleships.Strategies.Hit.BottomRowHitStrategy;
import Battleships.Strategies.Hit.HitStrategy;
import Battleships.Strategies.Hit.LeftColumnNotTopBottomLeftHitStrategy;
import Battleships.Strategies.Hit.NotOnEdgeHitStrategy;
import Battleships.Strategies.Hit.RightColumnNotTopBottomLeftHitStrategy;
import Battleships.Strategies.Hit.TopRowHitStrategy;

public class InfluenceMap implements Serializable {
	private int[][] map;
	private int maxVal;
	private static final int hit = 9;
	String coords;

	public InfluenceMap() {
		maxVal = 0;
		coords = "";
		map = new int[10][10];

		for (int a = 0; a < 10; a++)
			for (int b = 0; b < 10; b++)
				map[a][b] = 0;
	}

	public void set(Coordinate c, int value) {
		if (checkIndexBoundaries(c))
			throw new IllegalArgumentException("check index bound.");
		map[c.getI()][c.getJ()] = value;
	}

	public int getVal(int i, int j) {
		if (checkIndexBoundaries(new Coordinate(i, j)))
			throw new IllegalArgumentException("check index bound.");
		return map[i][j];
	}

	public boolean checkIndexBoundaries(Coordinate c) {
		return (c.getI() > 10 || c.getJ() > 10 || c.getI() < 0 || c.getJ() < 0);
	}

	public int getMaxHotspotVal() {
		maxVal = 0;
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (map[i][j] >= maxVal && map[i][j] != hit)
					maxVal = map[i][j];
			}
		}

		return maxVal;
	}

	public int getNumberOfHotspots() {
		int hs = 0;
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++)
				if ((map[i][j] == this.getMaxHotspotVal())
						&& (map[i][j] != hit) && (map[i][j] != 0)) {
					hs++;
				}
		}
		return hs;
	}

	public String getHotspots() {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (map[i][j] == maxVal)
					coords = coords + i + j;
			}
		}
		return coords;
	}

	public int[] getIntHotspots() {
		int[] refs = new int[this.getNumberOfHotspots() * 2];
		int ref1 = 0;
		int ref2 = 1;
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (map[i][j] == maxVal) {
					for (int x = 0; x < this.getNumberOfHotspots(); x++) {
						refs[ref1] = i;
						refs[ref2] = j;
					}
					ref1 = ref1 + 2;
					ref2 = ref2 + 2;
				}
			}
		}
		return refs;
	}

	public void hit(int i, int j) {
		map[i][j] = hit;
		SouthernHit(i, j);
		NorthernHit(i, j);
		EasternHit(i, j);
		WesternHit(i, j);
	}

	private void WesternHit(int i, int j) {
		try { // western is not a hit increment it
			if (map[i][j - 1] != hit)
				map[i][j - 1] = map[i][j - 1] + 4;

			// western is a hit and eastern isn't increment eastern by 8
			if (map[i][j - 1] == hit && map[i][j + 1] != hit) {
				map[i][j + 1] = map[i][j + 1] + 11;
			}
		}

		catch (ArrayIndexOutOfBoundsException e) {
			// do nothing
		}
	}

	private void EasternHit(int i, int j) {
		try { // if eastern is not a hit, increment it
			if (map[i][j + 1] != hit)
				map[i][j + 1] = map[i][j + 1] + 4;

			// if eastern is a hit, and western isn't increment western by 11
			if (map[i][j + 1] == hit && map[i][j - 1] != hit) {
				map[i][j - 1] = map[i][j - 1] + 11;
			}
		}

		catch (ArrayIndexOutOfBoundsException e) {
			// do nothing
		}
	}

	private void NorthernHit(int i, int j) {
		try { // if northern is not a hit, increament it
			if (map[i - 1][j] != hit) {
				map[i - 1][j] = map[i - 1][j] + 2;
			}

			// if northern is a hit and southern isn't then increment southern
			// by 8
			if (map[i - 1][j] == hit && map[i + 1][j] != hit) {
				map[i + 1][j] = map[i + 1][j] + 11;
			}
		}

		catch (ArrayIndexOutOfBoundsException e) {
			// do nothing
		}
	}

	private void SouthernHit(int i, int j) {
		try { // if southern is not a hit, increament it
			if (map[i + 1][j] != hit) {
				map[i + 1][j] = map[i + 1][j] + 2;
			}

			// if southern was also a hit and the northern isn't then increment
			// eastern by 5
			if (map[i + 1][j] == hit && map[i - 1][j] != hit) {
				map[i - 1][j] = map[i - 1][j] + 11;
			}

		}

		catch (ArrayIndexOutOfBoundsException e) {
			// do nothing
		}
	}

	public void sunk(int i, int j) {
		if (map[i][j] == hit) {

			HitStrategy hitStrategy = null;

			// if the hit is not on an edge
			if (i < 9 && i > 0 && j < 9 && j > 0) {
				hitStrategy = new NotOnEdgeHitStrategy();
			}
			// if hit is on left collumn but not top left or bottom left
			else if (j == 0 && i != 0 && i != 9) {
				hitStrategy = new LeftColumnNotTopBottomLeftHitStrategy();
			}
			// if hit is on right most column but not top right or bottom right
			else if (j == 9 && i != 0 && i != 9) {
				hitStrategy = new RightColumnNotTopBottomLeftHitStrategy();
			}

			// if hit on bottom row
			else if (i == 9) {
				hitStrategy = new BottomRowHitStrategy();
			}
			// if hit is on top row
			else if (i == 0) {
				hitStrategy = new TopRowHitStrategy();
			}
			hitStrategy.changeValueOfMapField(i, j, hit, map);
		}
	}

	/**
	 * Marks on the influence map where a miss is.
	 */
	public void miss(int i, int j) {
		map[i][j] = -5;

		EasternHits(i, j);
		WesternHits(i, j);

		SouthernHits(i, j);
		NorthenHits(i, j);

	}

	private void WesternHits(int i, int j) {
		try { // if i,j is a hit and north west, and south west are all misses
				// then set west to +9.
			if (coordHitNWSWmiss(i, j)) {
				map[i][j - 2] = map[i][j - 2] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // three hits to the left of a miss then east +2 is set to +9.
			if (threeHitsLeft(i, j)) {
				map[i][j - 3] = map[i][j - 3] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // j-1 and j-2 and j-3 are hits then j -4 is set to +9.
			if (j123HitsN(i, j)) {
				map[i][j - 4] = map[i][j - 4] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // j-1 and j-2 and j-3 and j-4 is set to +9.
			if (j1234HitN(i, j)) {
				map[i][j - 5] = map[i][j - 5] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}
	}

	private void EasternHits(int i, int j) {
		try { // if i,j is a hit and north east, and south east are all misses
				// then set east to +9.
			if (coordHitNESEmiss(i, j)) {
				map[i][j + 2] = map[i][j + 2] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
		}

		try { // north east and south east are both misses and east is a hit
				// then east +2 is set to +9.
			if (eastHitNESEmiss(i, j)) {
				map[i][j + 3] = map[i][j + 3] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // j+1 and j+2 and j+3 are hits then j +4 is set to +9.

			if (j123Hit(i, j)) {
				map[i][j + 4] = map[i][j + 4] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // j+1 and j+2 and j+3 and j+4 is set to +9.
			if (j1234Hit(i, j)) {
				map[i][j + 5] = map[i][j + 5] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}
	}

	private void NorthenHits(int i, int j) {
		try { // if i-1 is a hit and south, west, east are all misses then set
				// -2 to +9.
			if (i1HitSWEMiss(i, j)) {
				map[i - 2][j] = map[i - 2][j] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // if i-1 is a hit and i-2 is a hit and south, west, east are all
				// misses then set i-3 to +9.
			if (i12HitSWEMiss(i, j)) {
				map[i - 3][j] = map[i - 3][j] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // if i-1 is a hit and i-2 is a hit and i-3 is a hit then set i-4
				// to +9.
			if (i123NHit(i, j)) {
				map[i - 4][j] = map[i - 4][j] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // if i-1 is a hit and i-2 is a hit and i-3 and i-4 is a hit then
				// set i-5 to +9.
			if (i1234HN(i, j)) {
				map[i - 5][j] = map[i - 5][j] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}
	}

	private void SouthernHits(int i, int j) {
		try { // if i,j +1 is a hit and north, west, east are all misses then
				// set south to +9.
			if (hitNWEmiss(i, j)) {
				map[i + 2][j] = map[i + 2][j] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // if i+1 is a hit and i+2 is a hit and north, west, east are all
				// misses then set i+3 to +9.
			if (i12HitNWEMiss(i, j)) {
				map[i + 3][j] = map[i + 3][j] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // if i+1 is a hit and i+2 is a hit and i +3 is a hit then set i+4
				// to +9.
			if (i123Hits(i, j)) {
				map[i + 4][j] = map[i + 4][j] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}

		try { // if i+1 is a hit and i+2 is a hit and i +3 i+4 are hits then set
				// i + 5 to +9.
			if (i1234Hits(i, j)) {
				map[i + 5][j] = map[i + 5][j] + 9;
			}
		} catch (ArrayIndexOutOfBoundsException e) {/* do nothing */
		}
	}

	private boolean i1234HN(int i, int j) {
		return map[i - 1][j] == hit /* i-1 is hit */&& map[i - 2][j] == hit
				&& map[i - 3][j] == hit /* i-3 is hit */
				&& map[i - 4][j] == hit /* i-4 is hit */
				&& map[i - 5][j] != -5 /* i - 5 is not hit or miss */
				&& map[i - 5][j] != hit;
	}

	private boolean i123NHit(int i, int j) {
		return map[i - 1][j] == hit /* i-1 is hit */&& map[i - 2][j] == hit
				&& map[i - 3][j] == hit /* i-3 is hit */
				&& map[i - 4][j] != -5 /* i - 4 is not hit or miss */
				&& map[i - 4][j] != hit;
	}

	private boolean i12HitSWEMiss(int i, int j) {
		return map[i - 1][j] == hit /* i-1 is hit */&& map[i - 2][j] == hit
				&& map[i - 1][j - 1] == -5/* above left a miss */
				&& map[i - 1][j + 1] == -5 /* above right is a miss */
				&& map[i - 3][j] != -5 /* i - 3 is not hit or miss */
				&& map[i - 3][j] != hit;
	}

	private boolean i1HitSWEMiss(int i, int j) {
		return map[i - 1][j] == hit /* above is hit */
				&& map[i - 1][j - 1] == -5/* above left a miss */
				&& map[i - 1][j + 1] == -5 /* above right is a miss */
				&& map[i - 2][j] != -5 /* i - 2 is not hit or miss */
				&& map[i - 2][j] != hit;
	}

	private boolean i1234Hits(int i, int j) {
		return map[i + 1][j] == hit /* i+1 is a hit */&& map[i + 2][j] == hit
				&& map[i + 3][j] == hit /* i+ 3 is a hit */
				&& map[i + 4][j] == hit /* i+ 4 is a hit */
				&& map[i + 5][j] != -5 /* i + 3 is not hit or miss */
				&& map[i + 5][j] != hit;
	}

	private boolean i123Hits(int i, int j) {
		return map[i + 1][j] == hit /* i+1 is a hit */&& map[i + 2][j] == hit
				&& map[i + 3][j] == hit /* i+ 3 is a hit */
				&& map[i + 4][j] != -5 /* i + 3 is not hit or miss */
				&& map[i + 4][j] != hit;
	}

	private boolean i12HitNWEMiss(int i, int j) {
		return map[i + 1][j] == hit /* below is a hit */&& map[i + 2][j] == hit
				&& map[i + 1][j - 1] == -5/* below left a miss */
				&& map[i + 1][j + 1] == -5 /* below right is a miss */
				&& map[i + 3][j] != -5 /* i + 3 is not hit or miss */
				&& map[i + 3][j] != hit;
	}

	private boolean hitNWEmiss(int i, int j) {
		return map[i + 1][j] == hit /* below is hit */
				&& map[i + 1][j - 1] == -5/* below left a miss */
				&& map[i + 1][j + 1] == -5 /* below right is a miss */
				&& map[i + 2][j] != -5 /* i + 2 is not hit or miss */
				&& map[i + 2][j] != hit;
	}

	private boolean j1234HitN(int i, int j) {
		return map[i][j - 1] == hit /* east is a hit */&& map[i][j - 2] == hit
				&& map[i][j - 3] == hit /* east +3 is a hit */
				&& map[i][j - 4] == hit /* east +4 is a hit */
				&& map[i][j - 5] != hit && map[i][j - 5] != -5;
	}

	private boolean j123HitsN(int i, int j) {
		return map[i][j - 1] == hit /* j+1 is a hit */&& map[i][j - 2] == hit
				&& map[i][j - 3] == hit /* j +3 is a hit */
				&& map[i][j - 4] != -5 /* j +4 is not a hit or a miss */
				&& map[i][j - 4] != hit;
	}

	private boolean threeHitsLeft(int i, int j) {
		return map[i - 1][j - 1] == -5/* above left is a miss */
				&& map[i + 1][j - 1] == -5/* below left is a miss */
				&& map[i][j - 2] == hit /* left is a hit */
				&& map[i][j - 3] != -5 /* east + 3 is not a hit or miss */
				&& map[i][j - 3] != -5;
	}

	private boolean coordHitNWSWmiss(int i, int j) {
		return map[i - 1][j - 1] == -5/* above left a miss */
				&& map[i + 1][j - 1] == -5/* below left a miss */
				&& map[i][j - 1] == hit /* right is a hit */
				&& map[i][j - 2] != -5 /* j - 2 is not hit or miss */
				&& map[i][j - 2] != hit;
	}

	private boolean j1234Hit(int i, int j) {
		return map[i][j + 1] == hit /* east is a hit */&& map[i][j + 2] == hit
				&& map[i][j + 3] == hit /* east +3 is a hit */
				&& map[i][j + 4] == hit /* east +4 is a hit */
				&& map[i][j + 5] != hit && map[i][j + 5] != -5;
	}

	private boolean j123Hit(int i, int j) {
		return map[i][j + 1] == hit /* j+1 is a hit */&& map[i][j + 2] == hit
				&& map[i][j + 3] == hit /* j +3 is a hit */
				&& map[i][j + 4] != -5 /* j +4 is not a hit or a miss */
				&& map[i][j + 4] != hit;
	}

	private boolean eastHitNESEmiss(int i, int j) {
		return map[i - 1][j + 1] == -5/* above right is a miss */
				&& map[i + 1][j + 1] == -5/* below right is a miss */
				&& map[i][j + 2] == hit /* right is a hit */
				&& map[i][j + 3] != -5 /* east + 3 is not a hit or miss */
				&& map[i][j + 3] != -5;
	}

	private boolean coordHitNESEmiss(int i, int j) {
		return map[i - 1][j + 1] == -5/* above right a miss */
				&& map[i + 1][j + 1] == -5/* below right a miss */
				&& map[i][j + 1] == hit /* east is a hit */
				&& map[i][j + 2] != -5 /* east + 2 is not hit or miss */
				&& map[i][j + 2] != hit;
	}

	/**
	 * Returns the j of the cell with the highest influence value
	 */
	public int getHotspotJ() {
		int x = 0;
		int val = 0;

		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++) {
				if (map[i][j] > val && map[i][j] != hit) {
					val = map[i][j];
					x = j;
				}
			}
		return x;
	}

	/**
	 * Returns the i of the cell with the highest influence value
	 */
	public int getHotspotI() {
		int y = 0;
		int val = 0;

		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++) {
				if (map[i][j] > val && map[i][j] != hit) {
					val = map[i][j];
					y = i;
				}
			}
		return y;
	}

}