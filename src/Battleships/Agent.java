package Battleships;

import Battleships.Strategies.GenShot.GenShotCoordStrategy;
import Battleships.Strategies.GenShot.MultipleHSGenShotCoordStrategy;
import Battleships.Strategies.GenShot.OneHSGenShotCoordStrategy;
import Battleships.Strategies.GenShot.ZeroHSGenShotCoordStrategy;

public class Agent {

	private HomeGrid compHomeGrid;
	private Grid compAtt;
	private InfluenceMap influenceMap;
    private Coordinate coord;

	private GenShotCoordStrategy genShotCoordStrategy;

	public Agent() {
		influenceMap = new InfluenceMap();
		compHomeGrid = new HomeGrid();
		compAtt = new Grid();

		coord = new Coordinate(-1, -1);
	}

	public void nextShot() {

		if (influenceMap.getNumberOfHotspots() == 0) {
			genShotCoordStrategy = new ZeroHSGenShotCoordStrategy();
		}
		if (influenceMap.getNumberOfHotspots() == 1) {
			genShotCoordStrategy = new OneHSGenShotCoordStrategy();
		}
		if (influenceMap.getNumberOfHotspots() > 1) {
			genShotCoordStrategy = new MultipleHSGenShotCoordStrategy();
		}

		genShotCoordStrategy.setShotCoordinates(compAtt, influenceMap, coord);

	}

	public void placeShips() {
		while (!compHomeGrid.allShipsPlaced()) {
			NumberGenerator gen = new NumberGenerator();
			Coordinate c = new Coordinate(gen.rand(10), gen.rand(10));
			int o = gen.rand(2);

			compHomeGrid.getSubmarine().addShip(c, o, compHomeGrid);
			compHomeGrid.getBattleship().addShip(c, o, compHomeGrid);
			compHomeGrid.getAircraftCarrier().addShip(c, o, compHomeGrid);
			compHomeGrid.getMinesweeper().addShip(c, o, compHomeGrid);
			compHomeGrid.getDestroyer().addShip(c, o, compHomeGrid);
		}
	}

	public Coordinate getCoord() {
		return coord;
	}
	
	public Grid getCompAtt() {
		return compAtt;
	}

	public InfluenceMap getInfluenceMap() {
		return influenceMap;
	}

	public HomeGrid getCompHomeGrid() {
		return compHomeGrid;
	}

}
