package Battleships.Graphics;

/* File: DestroyerH.java	
 Instructions to draw a Destroyer on the screen.
 */

import java.awt.Color;
import java.awt.Graphics;

import Battleships.Coordinate;

public class DestroyerH extends ShipH {

	public void paint(Graphics g, Coordinate c) {
		int xLeft = c.getJ() * 20;
		int yTop = c.getI() * 20;

		Color navyGrey = new Color(180, 180, 180);

		// draw main body of ship
		g.setColor(navyGrey);
		g.fillOval(xLeft, yTop, 60, 20);
		// draw detail
		g.setColor(Color.black);

		g.drawOval(xLeft, yTop, 60, 20); // outline
		g.setColor(navyGrey);
		g.fillRect(xLeft + 20, yTop, 40, 20);

		g.setColor(Color.black);

		// Mid deck tower
		g.drawRect(xLeft + 40, yTop + 5, 15, 10);
		g.drawLine(xLeft + 40, yTop + 5, xLeft + 20, yTop + 10);
		g.drawLine(xLeft + 20, yTop + 10, xLeft + 40, yTop + 15);
		g.drawRect(xLeft + 45, yTop + 8, 5, 5);

		// Gun Barrel
		g.drawOval(xLeft + 5, yTop + 5, 10, 10);
		g.drawLine(xLeft, yTop + 10, xLeft + 10, yTop + 10);

		// Outline of the stern of the ship
		g.drawLine(xLeft + 20, yTop, xLeft + 60, yTop);
		g.drawLine(xLeft + 60, yTop, xLeft + 60, yTop + 20);
		g.drawLine(xLeft + 60, yTop + 20, xLeft + 20, yTop + 20);

	}

}