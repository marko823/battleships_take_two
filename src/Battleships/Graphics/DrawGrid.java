package Battleships.Graphics;
/*	File: Grid.java	
Instructions to draw a 10 by 10 grid on the screen.
*/
import java.awt.*;

public class DrawGrid
{	
	public static void paint(Graphics g, int xLeft, int yTop)
		{
			g.setColor(Color.black);
			g.drawRect(xLeft,yTop,200,200);
			
			for(int i=20; i<=200; i+=20){
				g.drawRect(xLeft,yTop,i,200);
			}
			for(int i=20; i<=200; i+=20){
				g.drawRect(xLeft,yTop,200,i);
			}			
		}

	
}
	
