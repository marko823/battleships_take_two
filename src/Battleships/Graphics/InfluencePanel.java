package Battleships.Graphics;
/*
 * Author: Michael
 * Created: 16 April 2005 17:25:31
 * Modified: 16 April 2005 17:25:31
 */

import javax.swing.*;

import Battleships.InfluenceMap;

import java.awt.*;

public class InfluencePanel extends JPanel
{
	public void paintComponent(Graphics g) {
		setSize(200, 200);
		super.paintComponent(g);
		DrawGrid.paint(g, 0, 0);
	}
}


