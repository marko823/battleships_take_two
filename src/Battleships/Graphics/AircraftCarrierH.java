package Battleships.Graphics;

/* File: AircraftCarrierH.java	
 Instructions to draw a AircraftCarrier horizontally on the screen.
 */

import java.awt.Color;
import java.awt.Graphics;

import Battleships.Coordinate;

public class AircraftCarrierH extends ShipH {
	public void paint(Graphics g, Coordinate c) {

		int xLeft = c.getJ() * 20;
		int yTop = c.getI() * 20;
		Color navyGrey = new Color(180, 180, 180);

		// draw main body of ship
		g.setColor(navyGrey);
		g.fillRect(xLeft, yTop, 100, 20);

		// draw detail
		g.setColor(Color.black);

		g.drawRect(xLeft, yTop, 100, 20); // outline
		// g.setColor(navyGrey);

		// Mid deck tower
		g.drawRect(xLeft + 40, yTop + 15, 20, 5);
		g.drawRect(xLeft + 45, yTop + 17, 10, 3);

		// Draw Runway

		g.setColor(Color.white);
		g.fillRect(xLeft + 3, yTop + 10, 10, 1);
		g.fillRect(xLeft + 15, yTop + 10, 10, 1);
		g.fillRect(xLeft + 28, yTop + 10, 10, 1);
		g.fillRect(xLeft + 40, yTop + 10, 10, 1);
		g.fillRect(xLeft + 53, yTop + 10, 10, 1);
		g.fillRect(xLeft + 65, yTop + 10, 10, 1);
		g.fillRect(xLeft + 78, yTop + 10, 10, 1);
		g.fillRect(xLeft + 90, yTop + 10, 10, 1);

	}

}