package Battleships.Graphics;

import java.awt.Graphics;

import Battleships.Coordinate;

public abstract class ShipV extends ShipGraphic{

	public abstract void paint(Graphics g, Coordinate c);
	
}
