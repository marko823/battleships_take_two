package Battleships;

import javax.swing.JTextField;

import Battleships.Graphics.AircraftCarrierH;
import Battleships.Graphics.AircraftCarrierV;
import Battleships.Graphics.BattleshipH;
import Battleships.Graphics.BattleshipV;
import Battleships.Graphics.DestroyerH;
import Battleships.Graphics.DestroyerV;
import Battleships.Graphics.MinesweeperH;
import Battleships.Graphics.MinesweeperV;
import Battleships.Graphics.Panel;
import Battleships.Graphics.SubmarineH;
import Battleships.Graphics.SubmarineV;
import Battleships.Ships.AircraftCarrier;
import Battleships.Ships.Battleship;
import Battleships.Ships.Destroyer;
import Battleships.Ships.Minesweeper;
import Battleships.Ships.Submarine;
import Battleships.Strategies.Shot.AircraftCarrierShotStrategy;
import Battleships.Strategies.Shot.BattleshipShotStrategy;
import Battleships.Strategies.Shot.DestroyerShotStrategy;
import Battleships.Strategies.Shot.MinesweeperShotStrategy;
import Battleships.Strategies.Shot.MissShotStrategy;
import Battleships.Strategies.Shot.NotValidShotStrategy;
import Battleships.Strategies.Shot.ShotStrategy;
import Battleships.Strategies.Shot.SubmarineShotStrategy;

public class HomeGrid extends Grid {

	private Minesweeper minesweeper;
	private Submarine submarine;
	private Destroyer destroyer;
	private Battleship battleship;
	private AircraftCarrier aircraftCarrier;

	public HomeGrid() {
		super();
		this.minesweeper = new Minesweeper();
		this.submarine = new Submarine();
		this.destroyer = new Destroyer();
		this.battleship = new Battleship();
		this.aircraftCarrier = new AircraftCarrier();
	}

	public Minesweeper getMinesweeper() {
		return minesweeper;
	}

	public Submarine getSubmarine() {
		return submarine;
	}

	public Destroyer getDestroyer() {
		return destroyer;
	}

	public Battleship getBattleship() {
		return battleship;
	}

	public AircraftCarrier getAircraftCarrier() {
		return aircraftCarrier;
	}

	public boolean allShipsSunk() {
		return getAircraftCarrier().isSunk() && getBattleship().isSunk()
				&& getDestroyer().isSunk() && getSubmarine().isSunk()
				&& getMinesweeper().isSunk();
	}

	public boolean allShipsPlaced() {
		return (minesweeper.isPlaced() && submarine.isPlaced()
				&& destroyer.isPlaced() && battleship.isPlaced() && aircraftCarrier
					.isPlaced());
	}

	public void setAllShipsSunk() {
		getAircraftCarrier().setSunk();
		getBattleship().setSunk();
		getDestroyer().setSunk();
		getMinesweeper().setSunk();
		getSubmarine().setSunk();
	}

	private boolean airPlaced() {
		return aircraftCarrier.isPlaced();
	}

	private boolean airBattlePlaced() {
		return airPlaced() && getBattleship().isPlaced();
	}

	private boolean airBattleDestPlaced() {
		return airBattlePlaced() && getDestroyer().isPlaced();
	}

	private boolean airBattleDestSubPlaced() {
		return airBattleDestPlaced() && getSubmarine().isPlaced();
	}

	// TODO
	public String deploy(Coordinate c, boolean horiz, Panel homePanel,
			JTextField outText, GUI gui) {
		String out1 = "";

		out1 = aircraftCarrier.placeShip(c, horiz, homePanel, outText, this,
				true, false, gui.getGameState(), new AircraftCarrierH(),
				new AircraftCarrierV());
		out1 = out1
				+ "\n"
				+ battleship.placeShip(c, horiz, homePanel, outText, this,
						airPlaced(), false, gui.getGameState(),
						new BattleshipH(), new BattleshipV());
		out1 = out1
				+ "\n"
				+ destroyer.placeShip(c, horiz, homePanel, outText, this,
						airBattlePlaced(), false, gui.getGameState(),
						new DestroyerH(), new DestroyerV());
		out1 = out1
				+ "\n"
				+ submarine.placeShip(c, horiz, homePanel, outText, this,
						airBattleDestPlaced(), false, gui.getGameState(),
						new SubmarineH(), new SubmarineV());
		out1 = out1
				+ "\n"
				+ minesweeper.placeShip(c, horiz, homePanel, outText, this,
						airBattleDestSubPlaced(), true, gui.getGameState(),
						new MinesweeperH(), new MinesweeperV());
		out1 = out1 + gui.guiDomain.gameState.playerTurn;

		return out1;
	}
	
	public boolean shot(Coordinate c) {
		ShotStrategy shotStrategy;
		if (getGridVal(c) == 0) {
			shotStrategy = new MissShotStrategy();
		} else if (getGridVal(c) == 2) {
			shotStrategy = new MinesweeperShotStrategy();
		} else if (getGridVal(c) == 3) {
			shotStrategy = new SubmarineShotStrategy();
		} else if (getGridVal(c) == 4) {
			shotStrategy = new BattleshipShotStrategy();
		} else if (getGridVal(c) == 5) {
			shotStrategy = new AircraftCarrierShotStrategy();
		} else if (getGridVal(c) == 7) {
			shotStrategy = new DestroyerShotStrategy();
		} else {
			shotStrategy = new NotValidShotStrategy();
		}
		return shotStrategy.updateShotCoorinate(c, this);

	}

}
