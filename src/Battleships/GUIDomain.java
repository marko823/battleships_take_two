package Battleships;

public class GUIDomain {

	protected GameState gameState;

	public boolean showMap;	
	public boolean horiz;
	public boolean agentWins;

	public GUIDomain(GameState gameState) {
		this.gameState = gameState;
	}

	public void initalizeParams() {
		showMap = true;
		horiz = true;
		agentWins = false;
	}

}
