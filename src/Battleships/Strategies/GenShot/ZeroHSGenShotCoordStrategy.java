package Battleships.Strategies.GenShot;

import Battleships.Coordinate;
import Battleships.Grid;
import Battleships.InfluenceMap;
import Battleships.NumberGenerator;

public class ZeroHSGenShotCoordStrategy extends GenShotCoordStrategy {

	@Override
	public void setShotCoordinates(Grid grid, InfluenceMap influenceMap, Coordinate coord) {
		NumberGenerator Powergen = new NumberGenerator();
		coord.setI(Powergen.rand(10));
		coord.setJ(Powergen.rand(10));
	}

}
