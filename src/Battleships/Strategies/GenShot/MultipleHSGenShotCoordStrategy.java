package Battleships.Strategies.GenShot;

import Battleships.Coordinate;
import Battleships.Grid;
import Battleships.InfluenceMap;
import Battleships.NumberGenerator;

public class MultipleHSGenShotCoordStrategy extends GenShotCoordStrategy {

	@Override
	public void setShotCoordinates(Grid compAtt, InfluenceMap influenceMap,
			Coordinate coord) {

		int[] refs = influenceMap.getIntHotspots();
		Coordinate refsC = new Coordinate(refs[0], refs[1]);

		if (compAtt.getGridVal(refsC) == 0) {
			coord.changeValue(refsC);
		} else {
			int loop = 0;
			boolean noneFound = false;
			while (compAtt.shipExistHere(coord) && !noneFound) {
				if (loop == 100)
					noneFound = true;
				for (int q = 2; q < refs.length - 1; q++) {
					coord.changeValue(new Coordinate(refs[q], refs[q + 1]));
				}
				loop++;
			}

		}

		for (int z = 0; z < refs.length - 2; z++) {
			refs[z] = refs[z + 2];
		}

	}

}
