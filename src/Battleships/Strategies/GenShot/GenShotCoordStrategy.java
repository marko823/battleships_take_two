package Battleships.Strategies.GenShot;

import Battleships.Coordinate;
import Battleships.Grid;
import Battleships.InfluenceMap;
import Battleships.NumberGenerator;

public abstract class GenShotCoordStrategy {	
	public abstract void setShotCoordinates(Grid grid, InfluenceMap influenceMap, Coordinate coord);
}
