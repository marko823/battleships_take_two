package Battleships.Strategies.GenShot;

import java.util.Random;

import Battleships.Coordinate;
import Battleships.Grid;
import Battleships.InfluenceMap;
import Battleships.NumberGenerator;

public class OneHSGenShotCoordStrategy extends GenShotCoordStrategy {
	@Override
	public void setShotCoordinates(Grid compAtt, InfluenceMap influenceMap,
			Coordinate coord) {
		Coordinate checkC = new Coordinate(influenceMap.getHotspotI(),
				influenceMap.getHotspotJ());

		if (compAtt.getGridVal(checkC) != 0) {
			influenceMap.set(checkC, 0);
		} else if (compAtt.getGridVal(checkC) == 0) {
		} else {
			Random generator = new Random();
			while (compAtt.getGridVal(checkC) != 0) {
				checkC = new Coordinate(generator.nextInt(10),
						generator.nextInt(10));
			}
		}
		coord.changeValue(checkC);
	}

}
