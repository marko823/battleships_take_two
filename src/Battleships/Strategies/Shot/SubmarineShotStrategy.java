package Battleships.Strategies.Shot;

import Battleships.Coordinate;
import Battleships.HomeGrid;

public class SubmarineShotStrategy extends ShotStrategy {

	@Override
	public boolean updateShotCoorinate(Coordinate c, HomeGrid grid) {
		grid.getSubmarine().scoreHit();
		if (grid.getSubmarine().isSunk() == true) {
		} else if (grid.getSubmarine().isSunk() == false) {
		}
		grid.update(c, (grid.getGridVal(c) - 8));
		return true;
	}

}
