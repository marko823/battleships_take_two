package Battleships.Strategies.Shot;

import Battleships.Coordinate;
import Battleships.HomeGrid;

public class NotValidShotStrategy extends ShotStrategy{

	@Override
	public boolean updateShotCoorinate(Coordinate c, HomeGrid grid) {			
		return false;
	}

}
