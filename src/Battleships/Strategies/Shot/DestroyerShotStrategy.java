package Battleships.Strategies.Shot;

import Battleships.Coordinate;
import Battleships.HomeGrid;

public class DestroyerShotStrategy extends ShotStrategy {

	@Override
	public boolean updateShotCoorinate(Coordinate c, HomeGrid grid) {
		grid.getDestroyer().scoreHit();

		if (grid.getDestroyer().isSunk() == true) {
		} else if (grid.getDestroyer().isSunk() == false) {
		}
		grid.update(c, (grid.getGridVal(c) - 8));
		return true;
	}

}
