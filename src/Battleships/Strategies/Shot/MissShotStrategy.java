package Battleships.Strategies.Shot;

import Battleships.Coordinate;
import Battleships.HomeGrid;

public class MissShotStrategy extends ShotStrategy{

	@Override
	public boolean updateShotCoorinate(Coordinate c, HomeGrid grid) {		
		grid.update(c, 1);
		return false;
	}

}
