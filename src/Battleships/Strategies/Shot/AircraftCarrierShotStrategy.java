package Battleships.Strategies.Shot;

import Battleships.Coordinate;
import Battleships.HomeGrid;

public class AircraftCarrierShotStrategy extends ShotStrategy {

	@Override
	public boolean updateShotCoorinate(Coordinate c, HomeGrid grid) {
		grid.getAircraftCarrier().scoreHit();

		if (grid.getAircraftCarrier().isSunk() == true) {
		} else if (grid.getAircraftCarrier().isSunk() == false) {
		}
		grid.update(c, (grid.getGridVal(c) - 8));		
		return true;
	}

}
