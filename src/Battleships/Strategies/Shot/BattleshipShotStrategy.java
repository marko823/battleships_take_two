package Battleships.Strategies.Shot;

import Battleships.Coordinate;
import Battleships.HomeGrid;

public class BattleshipShotStrategy extends ShotStrategy {

	@Override
	public boolean updateShotCoorinate(Coordinate c, HomeGrid grid) {
		grid.getBattleship().scoreHit();
		if (grid.getBattleship().isSunk() == true) {
		} else if (grid.getBattleship().isSunk() == false) {
		}
		grid.update(c, (grid.getGridVal(c) - 8));
		return true;
	}

}
