package Battleships.Strategies.Shot;

import Battleships.Coordinate;
import Battleships.HomeGrid;

public class MinesweeperShotStrategy extends ShotStrategy {

	@Override
	public boolean updateShotCoorinate(Coordinate c, HomeGrid grid) {
		grid.getMinesweeper().scoreHit();
		if (grid.getMinesweeper().isSunk() == true) {
		} else if (grid.getMinesweeper().isSunk() == false) {
		}
		grid.update(c, (grid.getGridVal(c) - 8));
		return true;
	}

}
