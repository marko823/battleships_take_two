package Battleships.Strategies.Shot;

import Battleships.Coordinate;
import Battleships.HomeGrid;

public abstract class ShotStrategy {
	
	public abstract boolean updateShotCoorinate(Coordinate c, HomeGrid grid);

}
