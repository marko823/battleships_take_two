package Battleships.Strategies.SunkOrShot;

import Battleships.Coordinate;
import Battleships.GUI;
import Battleships.GameState;

public class SubSunkOrShotStrategy extends SunkOrShotStrategy {

	@Override
	public void updateGridSetSunk(GameState gameState) {
		for (int i = 0; i < 10; i++) // change these to ROWS to use the
		// default
		{
			for (int j = 0; j < 10; j++)// change this to CoLumns for
			// default
			{
				if (gameState.playerHomeGrid.getGridVal(new Coordinate(i, j)) == -5) {
					gameState.agent.getInfluenceMap().sunk(i, j);
					gameState.playerHomeGrid.getSubmarine().setSunk();
				}
			}
		}

	}

}
