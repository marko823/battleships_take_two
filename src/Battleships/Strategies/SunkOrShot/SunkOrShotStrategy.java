package Battleships.Strategies.SunkOrShot;

import Battleships.GameState;

public abstract class SunkOrShotStrategy {
	
	public abstract void updateGridSetSunk(GameState gameState);

}
