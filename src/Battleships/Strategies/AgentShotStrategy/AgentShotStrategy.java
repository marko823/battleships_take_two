package Battleships.Strategies.AgentShotStrategy;

import Battleships.Coordinate;
import Battleships.GUI;
import Battleships.GameState;

public abstract class AgentShotStrategy {

	public abstract void updateShot(GameState data, Coordinate coord, GUI gui);
	
}
