package Battleships.Strategies.AgentShotStrategy;

import Battleships.Coordinate;
import Battleships.GUI;
import Battleships.GameState;
import Battleships.Graphics.MissIcon;

public class MissedAgentShotStrategy extends AgentShotStrategy {

	@Override
	public void updateShot(GameState gameState, Coordinate coord, GUI gui) {
		gameState.agent.getCompAtt().update(coord, 1);
		gameState.agent.getInfluenceMap().miss(coord.getI(), coord.getJ());
		MissIcon.paint(gui.homePanel.getGraphics(), (coord.getJ() * 20),
				(coord.getI() * 20));
		gameState.setTurn(true, false);
		gui.paintMap(gui.guiDomain.showMap);
		gui.getOutText().setText(gameState.turnToString());
	}

}
