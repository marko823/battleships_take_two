package Battleships.Strategies.AgentShotStrategy;

import Battleships.Coordinate;
import Battleships.GUI;
import Battleships.GameState;
import Battleships.Graphics.HitIcon;

public class HitAgentShotStrategy extends AgentShotStrategy {

	@Override
	public void updateShot(GameState gameState, Coordinate coord, GUI gui) {
		System.out.println(gameState.playerHomeGrid.shot(coord));
		gameState.agent.getCompAtt().update(coord, 8);
		gameState.agent.getInfluenceMap().hit(coord.getI(), coord.getJ());
		HitIcon.paint(gui.homePanel.getGraphics(), (coord.getJ() * 20), (coord.getI() * 20));
		gui.getOutText().setText("Agent Has Hit One Of your ships! Agent's Turn again");
		gui.paintMap(gui.guiDomain.showMap);		
	}	

}
