package Battleships.Strategies.Hit;

public class TopRowHitStrategy extends HitStrategy {

	@Override
	public void changeValueOfMapField(int i, int j, int hit, int[][] map) {
		// dec below if even
		if (map[i + 1][j] != hit && map[i + 1][j] % 2 == 0) {
			map[i + 1][j] = map[i + 1][j] - 2;
		}

		// dec below if odd
		if (map[i + 1][j] != hit && map[i + 1][j] % 2 == 1) {
			map[i + 1][j] = map[i + 1][j] - 9;
		}

		if (j != 0 && j != 9) {
			// dec right if even
			if (map[i][j + 1] != hit && map[i][j + 1] % 2 == 0) {
				map[i][j + 1] = map[i][j + 1] - 4;
			}

			// dec right if odd
			if (map[i][j + 1] != hit && map[i][j + 1] % 2 == 1) {
				map[i][j + 1] = map[i][j + 1] - 9;
			}

			// dec left if even
			if (map[i][j - 1] != hit && map[i][j - 1] % 2 == 0) {
				map[i][j - 1] = map[i][j - 1] - 4;
			}

			// dec left if odd
			if (map[i][j - 1] != hit && map[i][j - 1] % 2 == 1) {
				map[i][j - 1] = map[i][j - 1] - 9;
			}
		}

		// if in top left corner
		if (j == 0) {
			// dec right if even
			if (map[i][j + 1] != hit && map[i][j + 1] % 2 == 0) {
				map[i][j + 1] = map[i][j + 1] - 4;
			}

			// dec right if odd
			if (map[i][j + 1] != hit && map[i][j + 1] % 2 == 1) {
				map[i][j + 1] = map[i][j + 1] - 9;
			}
		}
		// if in top right corner
		if (j == 9) {
			// dec left if even
			if (map[i][j - 1] != hit && map[i][j - 1] % 2 == 0) {
				map[i][j - 1] = map[i][j - 1] - 4;
			}

			// dec left if odd
			if (map[i][j - 1] != hit && map[i][j - 1] % 2 == 1) {
				map[i][j - 1] = map[i][j - 1] - 9;
			}
		}
	}

}
