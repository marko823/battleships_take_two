package Battleships.Strategies.Hit;

public abstract class HitStrategy {
	
	public abstract void changeValueOfMapField(int i, int j, int hit, int[][] map);

}
