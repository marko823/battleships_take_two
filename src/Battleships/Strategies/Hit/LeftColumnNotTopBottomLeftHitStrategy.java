package Battleships.Strategies.Hit;

public class LeftColumnNotTopBottomLeftHitStrategy extends HitStrategy {

	@Override
	public void changeValueOfMapField(int i, int j, int hit, int[][] map) {
		if (map[i][j + 1] != hit && map[i][j + 1] % 2 == 0) {
			map[i][j + 1] = map[i][j + 1] - 4;
		}

		// dec to right if odd
		if (map[i][j + 1] != hit && map[i][j + 1] % 2 == 1) {
			map[i][j + 1] = map[i][j + 1] - 9;// 4
		}

		if (i != 0 && i != 9) {
			// dec above if even
			if (map[i - 1][j] != hit && map[i - 1][j] % 2 == 0) {
				map[i - 1][j] = map[i - 1][j] - 2;
			}

			// dec above if odd
			if (map[i - 1][j] != hit && map[i - 1][j] % 2 == 1) {
				map[i - 1][j] = map[i - 1][j] - 9; // -2
			}

			// dec below of even
			if (map[i + 1][j] != hit && map[i + 1][j] % 2 == 0) {
				map[i + 1][j] = map[i + 1][j] - 2;
			}

			// dec below of odd
			if (map[i + 1][j] != hit && map[i + 1][j] % 2 == 1) {
				map[i + 1][j] = map[i + 1][j] - 9; // -2
			}
		}

	}

}
