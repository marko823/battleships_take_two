package Battleships.Strategies.Hit;

public class NotOnEdgeHitStrategy extends HitStrategy {

	@Override
	public void changeValueOfMapField(int i, int j, int hit, int[][] map) {
		// dec above if odd
		if (map[i - 1][j] != hit && map[i - 1][j] % 2 == 1) {
			// if(map[i-1][j]==13)
			map[i - 1][j] = map[i - 1][j] - 9;
		}

		// dec above if even
		if (map[i - 1][j] != hit && map[i - 1][j] % 2 == 0) {
			map[i - 1][j] = map[i - 1][j] - 2;
		}

		// dec below if odd
		if (map[i + 1][j] != hit && map[i + 1][j] % 2 == 1) {
			map[i + 1][j] = map[i + 1][j] - 9;
		}

		// dec below if even
		if (map[i + 1][j] != hit && map[i + 1][j] % 2 == 0) {
			map[i + 1][j] = map[i + 1][j] - 2;
		}

		// dec left if even
		if (map[i][j - 1] != hit && map[i][j - 1] % 2 == 0) {
			map[i][j - 1] = map[i][j - 1] - 4;
		}

		// dec left if odd
		if (map[i][j - 1] != hit && map[i][j - 1] % 2 == 1) {
			map[i][j - 1] = map[i][j - 1] - 9;
		}

		// dec right if even
		if (map[i][j + 1] != hit && map[i][j + 1] % 2 == 0) {
			map[i][j + 1] = map[i][j + 1] - 4;
		}

		// dec right if odd
		if (map[i][j + 1] != hit && map[i][j + 1] % 2 == 1) {
			map[i][j + 1] = map[i][j + 1] - 9;
		}
		
	}

}
