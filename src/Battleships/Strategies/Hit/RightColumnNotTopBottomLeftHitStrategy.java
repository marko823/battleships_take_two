package Battleships.Strategies.Hit;

public class RightColumnNotTopBottomLeftHitStrategy extends HitStrategy {

	@Override
	public void changeValueOfMapField(int i, int j, int hit, int[][] map) {
		// dec to left if even
		if (map[i][j - 1] != hit && map[i][j - 1] % 2 == 0) {
			map[i][j - 1] = map[i][j - 1] - 4;
		}

		// dec to left if odd
		if (map[i][j - 1] != hit && map[i][j - 1] % 2 == 1) {
			map[i][j - 1] = map[i][j - 1] - 9;
		}

		if (i != 0 && i != 9) {
			// dec above if even
			if (map[i - 1][j] != hit && map[i - 1][j] % 2 == 0) {
				map[i - 1][j] = map[i - 1][j] - 2;
			}

			// dec above if odd
			if (map[i - 1][j] != hit && map[i - 1][j] % 2 == 1) {
				map[i - 1][j] = map[i - 1][j] - 9;
			}

			// dec below if even
			if (map[i + 1][j] != hit && map[i + 1][j] % 2 == 0) {
				map[i + 1][j] = map[i + 1][j] - 2;
			}

			// dec below if odd
			if (map[i + 1][j] != hit && map[i + 1][j] % 2 == 1) {
				map[i + 1][j] = map[i + 1][j] - 9;
			}
		}

	}

}
