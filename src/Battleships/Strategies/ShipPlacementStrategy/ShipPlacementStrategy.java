package Battleships.Strategies.ShipPlacementStrategy;

import javax.swing.JTextField;

import Battleships.Coordinate;
import Battleships.Grid;
import Battleships.Graphics.Panel;
import Battleships.Graphics.ShipGraphic;
import Battleships.Ships.Ship;

public abstract class ShipPlacementStrategy {

	public abstract String placeShip(Coordinate c, Ship ship,
			JTextField outText, Panel homePanel, Grid grid, ShipGraphic shipG);

}
