package Battleships.Strategies.ShipPlacementStrategy;

import javax.swing.JTextField;

import Battleships.Coordinate;
import Battleships.Grid;
import Battleships.Graphics.Panel;
import Battleships.Graphics.ShipGraphic;
import Battleships.Ships.Ship;

public class HorizShipPlacementStrategy extends ShipPlacementStrategy {

	@Override
	public String placeShip(Coordinate c, Ship ship, JTextField outText,
			Panel homePanel, Grid grid, ShipGraphic shipH) {
		String out = "";
		ship.addShip(c, 0, grid);
		if (ship.isPlaced()) {
			shipH.paint(homePanel.getGraphics(), c);
			ship.setPlaced(true);
			outText.setText("Air Placed");
		} else {
			outText.setText("Aircraft Carrier Will Not Fit Here");
			out = "not valid";
		}
		return out;
	}

}
