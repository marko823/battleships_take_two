package Battleships.Strategies.ShipPlacementStrategy;

import javax.swing.JTextField;

import Battleships.Coordinate;
import Battleships.Grid;
import Battleships.Graphics.Panel;
import Battleships.Graphics.ShipGraphic;
import Battleships.Graphics.ShipH;
import Battleships.Ships.Ship;

public class VerticalShipPlacementStrategy extends ShipPlacementStrategy {

	@Override
	public String placeShip(Coordinate c, Ship ship, JTextField outText,
			Panel homePanel, Grid grid, ShipGraphic shipV) {
		String out = "";
		ship.addShip(c, 1, grid);
		if (ship.isPlaced()) {
			shipV.paint(homePanel.getGraphics(), c);
			ship.setPlaced(true);
			outText.setText("Air Placed");
		} else {
			out = "not valid";
		}
		return out ;
	}

}
