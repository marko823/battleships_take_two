package Battleships;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import Battleships.Graphics.HitIcon;
import Battleships.Graphics.InfluenceMapGraphic;
import Battleships.Graphics.InfluencePanel;
import Battleships.Graphics.MissIcon;
import Battleships.Graphics.Panel;
import Battleships.MouseAdapters.AttackMousePressListener;
import Battleships.MouseAdapters.HideButtonAction;
import Battleships.MouseAdapters.HomeMousePressListener;
import Battleships.MouseAdapters.QuitButtonAction;
import Battleships.MouseAdapters.RotateButtonAction;
import Battleships.MouseAdapters.ShowButtonAction;

public class GUI extends JFrame {

	public GUIDomain guiDomain;

	public Panel attackPanel;
	public Panel homePanel;
	public InfluencePanel influenceMapPanel;
	public JTextField outText;

	public GUI(GameState gameState) {
		guiDomain = new GUIDomain(gameState);
		guiDomain.initalizeParams();
		initalizeGUIElements();
	}

	private void initalizeGUIElements() {

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container contentPane = this.getContentPane();
		contentPane.setLayout(new BorderLayout(2, 1));
		this.setResizable(false);

		// attack panel
		Container APanel = new Container();
		APanel.setLayout(new GridLayout());

		// home panel
		Container HPanel = new Container();
		HPanel.setLayout(new GridLayout());

		// influence map
		Container IMPanel = new Container();
		IMPanel.setLayout(new GridLayout());

		// center panel where grids are
		Container CenterPanel = new Container();
		CenterPanel.setLayout(new GridLayout(1, 3));
		// create grids and maps

		// attack panel add listener
		attackPanel = new Panel();
		attackPanel.addMouseListener(new AttackMousePressListener(attackPanel,
				this));

		homePanel = new Panel();
		homePanel.addMouseListener(new HomeMousePressListener(homePanel, this));

		influenceMapPanel = new InfluencePanel();

		APanel.add(attackPanel);
		CenterPanel.add(APanel);

		HPanel.add(homePanel);
		CenterPanel.add(HPanel);

		IMPanel.add(influenceMapPanel);
		CenterPanel.add(IMPanel);

		Container southPanel = new Container();
		southPanel.setLayout(new GridLayout(1, 2));
		southPanel.setSize(400, 400);

		Container shipPanel = new Container();
		shipPanel.setLayout(new GridLayout(4, 2));
		// adds the Ship Panel to the south panel
		southPanel.add(shipPanel);

		Container topShipPanel = new Container();
		topShipPanel.setLayout(new FlowLayout());
		// add topShipPanel to shipPanel
		shipPanel.add(topShipPanel);

		Container topShipLabelPanel = new Container();
		topShipLabelPanel.setLayout(new FlowLayout());
		// add topShipPanel to shipPanel
		shipPanel.add(topShipLabelPanel);

		Container bottomShipPanel = new Container();
		bottomShipPanel.setLayout(new FlowLayout());
		// add bottomShipPanel to shipPanel
		shipPanel.add(bottomShipPanel);

		Container bottomShipLabelPanel = new Container();
		bottomShipLabelPanel.setLayout(new FlowLayout());
		// add bottomShipPanel to shipPanel
		shipPanel.add(bottomShipLabelPanel);

		JButton NewButton = new JButton("New Game");
		topShipPanel.add(NewButton);
		// NewButton.addMouseListener(new NewButtonAction(this));

		JButton hideButton = new JButton("Hide Influence Map");
		topShipPanel.add(hideButton);
		hideButton.addMouseListener(new HideButtonAction(this));

		JButton destButton = new JButton("Rotate");
		topShipPanel.add(destButton);

		JButton rotateButton = new JButton("Rotate Ship");
		rotateButton.addMouseListener(new RotateButtonAction(this));
		bottomShipPanel.add(rotateButton);

		JButton quitButton = new JButton("Quit");
		quitButton.addMouseListener(new QuitButtonAction());
		bottomShipPanel.add(quitButton);

		Container rotatePanel = new Container();
		rotatePanel.setLayout(new BorderLayout());
		// add rotatePanel to southPanel
		southPanel.add(rotatePanel);

		JButton viewMap = new JButton("View Influence Map");
		viewMap.addMouseListener(new ShowButtonAction(this));
		rotatePanel.add(viewMap, BorderLayout.NORTH);

		outText = new JTextField("lookat me!");
		getOutText().setText(
				"Welcome To Battleships. Place ships on the middle grid");
		getOutText().setEditable(false);
		rotatePanel.add(getOutText());

		contentPane.add(CenterPanel, BorderLayout.CENTER);
		contentPane.add(southPanel, BorderLayout.SOUTH);

		this.pack();
		this.setSize(640, 400);
		this.setVisible(true);
	}

	public void repaintAttackPanel() {
		for (int i = 0; i < 10; i++) // change these to ROWS to use the default
		{
			for (int j = 0; j < 10; j++)// change this to CoLumns for default
			{
				Coordinate c = new Coordinate(i, j);
				if (getGameState().isMissRepaint(c))
					MissIcon.paint(attackPanel.getGraphics(), (j * 20),
							(i * 20));
				else if (getGameState().isHitRepaint(c))
					HitIcon.paint(attackPanel.getGraphics(), (j * 20), (i * 20));

			}
		}
	}

	public void rotate() {
		guiDomain.horiz = !getHoriz();
		if (getHoriz() && !getGameState().allShipsDeployed())
			getOutText().setText("Ship Will Be Placed Horizontally");
		if (!getHoriz() && !getGameState().allShipsDeployed())
			getOutText().setText("Ship Will Be Placed Vertically");
	}

	public void showMap() {
		paintMap(true);
		getOutText().setText("Influence Map Shown");
	}

	public void hideMap() {
		paintMap(false);
		getOutText().setText("Influence Map Hidden");
	}

	public void paintMap(boolean showMap) {
		guiDomain.showMap = showMap;

		for (int i = 0; i < 10; i++) // change these to ROWS to use the default
		{
			for (int j = 0; j < 10; j++)// change this to CoLumns for default
			{
				if (showMap) {
					InfluenceMapGraphic.paint(influenceMapPanel.getGraphics(),
							(j * 20), (i * 20), getGameState().agent
									.getInfluenceMap().getVal(i, j));
				} else {
					InfluenceMapGraphic.paint(influenceMapPanel.getGraphics(),
							(j * 20), (i * 20), 0);
				}
			}
		}

	}
	
	public static int resolveAxisCoord(int x) {
		if(x<200)
			return x / 20;
		return -1;
	}

	public JTextField getOutText() {
		return outText;
	}

	public void setOut(String s) {
		outText.setText(s);
	}

	//delegating methods 
	public void setAgentWins(boolean agentWins) {
		guiDomain.agentWins = agentWins;
	}

	public boolean getHoriz() {
		return guiDomain.horiz;
	}

	public GameState getGameState() {
		return guiDomain.gameState;
	}

}
