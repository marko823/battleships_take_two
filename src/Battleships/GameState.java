package Battleships;

import java.awt.Graphics;

import javax.swing.JTextField;

import Battleships.Graphics.HitIcon;
import Battleships.Graphics.MissIcon;
import Battleships.Strategies.AgentShotStrategy.AgentShotStrategy;
import Battleships.Strategies.AgentShotStrategy.HitAgentShotStrategy;
import Battleships.Strategies.AgentShotStrategy.MissedAgentShotStrategy;
import Battleships.Strategies.SunkOrShot.AirSunkOrShotStrategy;
import Battleships.Strategies.SunkOrShot.BattleSunkOrShotStrategy;
import Battleships.Strategies.SunkOrShot.DestSunkOrShotStrategy;
import Battleships.Strategies.SunkOrShot.MineSunkOrShotStrategy;
import Battleships.Strategies.SunkOrShot.SubSunkOrShotStrategy;
import Battleships.Strategies.SunkOrShot.SunkOrShotStrategy;

public class GameState {

	private boolean gameOver;
	public Agent agent;

	public Grid playerAtt;
	public HomeGrid playerHomeGrid;

	public boolean playerTurn;
	public boolean agentTurn;

	public boolean playerWins;
	public boolean playerShipsdeployed;

	public static SunkOrShotStrategy sunkOrShotStrategy;

	public GameState() {
		gameOver = false;
		agent = new Agent();

		playerAtt = new Grid();
		playerHomeGrid = new HomeGrid();

		playerTurn = true;
		agentTurn = false;

		playerWins = false;
		playerShipsdeployed = false;
	}

	public void acceptPlayerShot(Coordinate c, Graphics attackPanelGraphics,
			JTextField outText) {

		if (playerAtt.getGridVal(c) == 0) {

			if (agent.getCompHomeGrid().shot(c)) {
				HitIcon.paint(attackPanelGraphics, (c.getJ() * 20),
						(c.getI() * 20));
				playerAtt.update(c, 9);
				outText.setText("HIT! Have Another Turn!");
			} else {
				MissIcon.paint(attackPanelGraphics, (c.getJ() * 20),
						(c.getI() * 20));
				agent.getCompHomeGrid().update(c, 1);
				playerAtt.update(c, 1);
				outText.setText("Miss. Agent's Turn");
				setTurn(true, false);
			}

		}

		agent.getCompHomeGrid().setAllShipsSunk();
	}

	public boolean allShipsDeployed() {
		return playerShipsdeployed && agent.getCompHomeGrid().allShipsPlaced();
	}

	public boolean IsAcceptingPlayerInput() {
		return !gameOver && playerShipsdeployed && playerTurn;
	}

	public boolean isMissRepaint(Coordinate c) {
		return playerAtt.getGridVal(c) == 1;
	}

	public boolean isHitRepaint(Coordinate c) {
		return agent.getCompHomeGrid().getGridVal(c) < -1;
	}

	public void setTurn(boolean agent, boolean player) {
		agentTurn = agent;
		playerTurn = player;
	}

	public String turnToString() {
		if (playerTurn)
			return "Player turn, take a shot";
		else
			return "Agent turn, please wait";
	}

	public void playerOnMove() {
		while (playerTurn && !gameOver) {
			agent.getCompHomeGrid().setAllShipsSunk();
			if (agent.getCompHomeGrid().allShipsSunk()) {
				gameOver = true;
				playerWins = true;
			}
		}
	}

	public void didAgentSunkOrShotShip(GUI gui) {

		if (playerHomeGrid.getMinesweeper().isSunk()) {
			sunkOrShotStrategy = new MineSunkOrShotStrategy();
			sunkOrShotStrategy.updateGridSetSunk(this);
		}
		if (playerHomeGrid.getDestroyer().isSunk()) {
			sunkOrShotStrategy = new DestSunkOrShotStrategy();
			sunkOrShotStrategy.updateGridSetSunk(this);
		}
		if (playerHomeGrid.getSubmarine().isSunk()) {
			sunkOrShotStrategy = new SubSunkOrShotStrategy();
			sunkOrShotStrategy.updateGridSetSunk(this);
		}
		if (playerHomeGrid.getBattleship().isSunk()) {
			sunkOrShotStrategy = new BattleSunkOrShotStrategy();
			sunkOrShotStrategy.updateGridSetSunk(this);
		}
		if (playerHomeGrid.getAircraftCarrier().isSunk()) {
			sunkOrShotStrategy = new AirSunkOrShotStrategy();
			sunkOrShotStrategy.updateGridSetSunk(this);
		}
	}

	public void endDeploymentPhase(JTextField outText) {
		playerShipsdeployed = true;
		setTurn(false, true);
		outText.setText(turnToString());
	}

	public void agentShot(GUI gui) {

		AgentShotStrategy agentShotStretegy;
		if (agentTurn && allShipsDeployed()) {

			if (playerHomeGrid.getGridVal(agent.getCoord()) == 0) {
				agentShotStretegy = new MissedAgentShotStrategy();
				agentShotStretegy.updateShot(this, agent.getCoord(), gui);
				setTurn(false, true);
			}
			if (playerHomeGrid.getGridVal(agent.getCoord()) > 1) {
				agentShotStretegy = new HitAgentShotStrategy();
				agentShotStretegy.updateShot(this, agent.getCoord(), gui);
			}
		}
	}
	
	public void agentOnMove(GUI gui) {
		agent.nextShot();				
		agentShot(gui);
		didAgentSunkOrShotShip(gui);
		agent.getCompHomeGrid().setAllShipsSunk();
	}

	public boolean isPlayerWins() {
		return playerWins;
	}

	public boolean isAgentTurn() {
		return agentTurn;
	}

	public boolean IsGameOver() {
		return gameOver;
	}

	public void SetGameOver(boolean value) {
		gameOver = value;
	}

}