package Battleships;

import java.io.Serializable;

public class Grid implements Serializable {

	public static final int HeightOfGrid = 10;
	public static final int WidthOfGrid = 10;

	private int[][] board;

	public Grid() {
		board = new int[HeightOfGrid][WidthOfGrid];

		for (int a = 0; a < HeightOfGrid; a++)
			for (int b = 0; b < WidthOfGrid; b++)
				board[a][b] = 0;
	}

	public boolean shipExistHere(Coordinate coord) {
		return (getGridVal(coord) > 1 && getGridVal(coord) < 8);
	}

	public void update(Coordinate c, int value) {
		board[c.getI()][c.getJ()] = value;
	}

	public int getGridVal(Coordinate c) {
		return board[c.getI()][c.getJ()];
	}

	

}
