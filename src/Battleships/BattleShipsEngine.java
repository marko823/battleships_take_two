package Battleships;

public class BattleShipsEngine {

	public static GameState gameState;
	public static GUI gui;

	public static void main(String args[]) {

		initalizeGame();
		while (!gameState.IsGameOver()) {

			gameState.playerOnMove();
			gui.repaintAttackPanel();
			while (gameState.isAgentTurn() && !gameState.IsGameOver()) {

				gameState.agentOnMove(gui);

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (gameState.playerHomeGrid.allShipsSunk()) {
					gui.setAgentWins(true);
					gameState.SetGameOver(true);
					gameState.setTurn(false, true);
				}
			}
		}
		showResult(gui);
	}

	private static void initalizeGame() {

		gameState = new GameState();
		gui = new GUI(gameState);

		while (!gameState.playerHomeGrid.allShipsPlaced()) {
			// PlayerDeploymentPhase, wait for player to place all their ships
		}

		gameState.agent.placeShips();
		gameState.setTurn(false, true);
		gui.outText.setText(gameState.turnToString());
	}

	private static void showResult(GUI gui) {
		System.out.println("Game Over!");
		if (gui.guiDomain.gameState.isPlayerWins()) {
			System.out.println("Player Wins");
			gui.setOut("Game Over! You Win!");
		} else {
			System.out.println("Computer Wins");
			gui.setOut("Game Over! Agent Wins!");
		}
	}

}