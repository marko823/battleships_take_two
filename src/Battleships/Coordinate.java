package Battleships;

public class Coordinate {
	
	private int i;
	private int j;
	
	public Coordinate(int i, int j) {		
		this.i = i;
		this.j = j;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}
	
	public void changeValue(Coordinate c){
		this.i = c.getI();
		this.j = c.getJ();
	}	
}
