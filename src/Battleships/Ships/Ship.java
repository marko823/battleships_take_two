package Battleships.Ships;

/*
 * Author: Michael
 * Created: 07 December 2004 15:52:31
 * Modified: 07 December 2004 15:52:31
 */

import java.io.Serializable;

import javax.swing.JTextField;

import Battleships.Coordinate;
import Battleships.GameState;
import Battleships.Grid;
import Battleships.Exception.PositionExceedsBoardException;
import Battleships.Exception.PositionOccupiedException;
import Battleships.Graphics.Panel;
import Battleships.Graphics.ShipH;
import Battleships.Graphics.ShipV;
import Battleships.Strategies.ShipPlacementStrategy.HorizShipPlacementStrategy;
import Battleships.Strategies.ShipPlacementStrategy.ShipPlacementStrategy;
import Battleships.Strategies.ShipPlacementStrategy.VerticalShipPlacementStrategy;

public abstract class Ship implements Serializable {
	protected int intactSegments;
	protected boolean isPlaced;
	protected boolean isSunk;

	public Ship(int intactSegments) {
		this.intactSegments = intactSegments;
		this.isPlaced = false;
	}

	public void setSunk() {
		isSunk = isSunk();
	}

	public boolean isSunk() {
		return (intactSegments == 0);
	}

	public boolean isPlaced() {
		return isPlaced;
	}

	public void setPlaced(boolean isPlaced) {
		this.isPlaced = isPlaced;
	}

	public void scoreHit() {
		intactSegments--;
		if (intactSegments < 0)
			throw new IllegalArgumentException("Segments var is less than 0");
	}

	public void placeShipOnGrid(Grid board, Coordinate coord,
			boolean isHorizontal) {
		if (isPlaced()) {
			System.out.println(this.getClass().getName() + " already placed\n");
			return;
		} else if (isHorizontal) {
			placeHorizontalShipOnGrid(board, coord, board.HeightOfGrid);
		} else {
			placeVerticalShipOnGrid(board, coord, board.WidthOfGrid);
		}
	}

	private void placeVerticalShipOnGrid(Grid board, Coordinate coord,
			int userRow) {
		if (coord.getI() + intactSegments > userRow)
			throw new PositionExceedsBoardException();

		for (int r = coord.getI(); r < coord.getI() + intactSegments; r++)
			while (board.getGridVal(new Coordinate(r, coord.getJ())) != 0) {
				throw new PositionOccupiedException();
			}

		for (int r = coord.getI(); r < coord.getI() + intactSegments; r++) {
			board.update(new Coordinate(r, coord.getJ()), shipGridValue(this));
		}
		// board.setShipAsPlaced(this);
		setPlaced(true);
	}

	private void placeHorizontalShipOnGrid(Grid board, Coordinate coord,
			int userColumn) {
		if (coord.getJ() + intactSegments > userColumn)
			throw new PositionExceedsBoardException();

		for (int c = coord.getJ(); c < coord.getJ() + intactSegments; c++)
			while (board.getGridVal(new Coordinate(coord.getI(), c)) != 0) {
				throw new PositionOccupiedException();
			}

		for (int c = coord.getJ(); c < coord.getJ() + intactSegments; c++) {
			board.update(new Coordinate(coord.getI(), c), shipGridValue(this));
		}
		// board.setShipAsPlaced(this);
		setPlaced(true);
	}

	private int shipGridValue(Ship ship) {
		Class<? extends Ship> shipclass = ship.getClass();
		if (shipclass.equals(AircraftCarrier.class))
			return 5;
		if (shipclass.equals(Battleship.class))
			return 4;
		if (shipclass.equals(Destroyer.class))
			return 7;
		if (shipclass.equals(Submarine.class))
			return 3;
		if (shipclass.equals(Minesweeper.class))
			return 2;
		return 9;
	}

	public void addShip(Coordinate c, int s, Grid grid) {
		try {
			placeShipOnGrid(grid, c, (s == 0));
		}

		catch (PositionOccupiedException Exception) {
			System.out
					.println(String
							.format("Cannot place %s Minesweeper here, position is occupied \n",
									((s == 0) ? "horizontal" : "vertical")));
		}

		catch (PositionExceedsBoardException Exception) {
			System.out
					.println(String
							.format("Cannot place %s Minesweeper here, ship will not fit on grid \n",
									((s == 0) ? "horizontal" : "vertical")));
		}
	}

	public String placeShip(Coordinate c, boolean horiz, Panel homePanel,
			JTextField outText, Grid grid, boolean othersPlaced,
			boolean allPlaced, GameState gs, ShipH shipH, ShipV shipV) {
		String out = "";
		if (!this.isPlaced() && othersPlaced) {
			ShipPlacementStrategy shipPlacementStretegy;
			if (horiz) {
				shipPlacementStretegy = new HorizShipPlacementStrategy();
				shipPlacementStretegy.placeShip(c, this, outText, homePanel, grid, shipH);
			}
			else {				
				shipPlacementStretegy = new VerticalShipPlacementStrategy();
				shipPlacementStretegy.placeShip(c, this, outText, homePanel, grid, shipV);
			}
			
			out = out + toString();
		}

		if (allPlaced)
			gs.endDeploymentPhase(outText);

		return out;
	}

}
