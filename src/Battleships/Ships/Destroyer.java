package Battleships.Ships;

/*
 * Author: Michael
 * Created: 07 December 2004 16:50:18
 * Modified: 07 December 2004 16:50:18
 */

import java.io.Serializable;

public class Destroyer extends Ship implements Serializable {
	public Destroyer() {
		super(3);
	}
}
