package Battleships.Ships;

/*
 * Author: Michael
 * Created: 08 December 2004 09:37:10
 * Modified: 08 December 2004 09:37:10
 */

import java.io.Serializable;

public class AircraftCarrier extends Ship implements Serializable {
	public AircraftCarrier() {
		super(5);
	}
}
