package Battleships.Ships;

/*
 * Author: Michael
 * Created: 07 December 2004 23:01:02
 * Modified: 07 December 2004 23:01:02
 */

import java.io.Serializable;

public class Battleship extends Ship implements Serializable {
	public Battleship() {
		super(4);
	}
}
